<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Auth::routes();

Route::get('/home', [App\Http\Controllers\ArticleController::class, 'index'])->name('home');

Route::get('/article/create',[App\Http\Controllers\ArticleController::class, 'create']);
Route::post('/article/store',[App\Http\Controllers\ArticleController::class, 'store']);
Route::get('/article/filter/{category_id}',[App\Http\Controllers\ArticleController::class, 'filter']);

Auth::routes();

//Route::resource('comment', 'App\Http\Controllers\CommentController');

Route::post('/comment/{id}',[App\Http\Controllers\CommentController::class, 'store']);
