@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-lg-6">
                            @role('admin')
                                <a href="/article/create"> Add Article </a>
                            @endrole
                        </div>
                        <div class="col-lg-6">
                            <select name="category_id" class="input1" onchange="filterArticles(this.options[this.selectedIndex].value)" style="float:right">
                                        <option value="0">All</option>
                                        @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                            </select>
                        </div>
                    </div>
                    <div id="articles">
                    @foreach($articles as $article)
                        <div class="card" style="margin-top:1%">
                            <div class="card-header">
                                {{$article->title}}
                            </div>
                            <div class="card-body">
                                {{$article->content}}
                            </div>
                            <div class="card-bottom">
                                <button onclick="addComments({{$article->id}})"> Add Comment </button>
                            </div>
                         

                            <div id="comments{{$article->id}}" style="display:none">
                                <hr>
                                <div class="row add-comment">
                                    <div class="col-md-12" style="line-height:80%">
                                        <form action="/comment/{{$article->id}}" method="POST">
                                        @csrf
                                            <input id="commentQuestion{{$article->id}}" class="input1 new_comment" type="text" name="content" placeholder="add your comment">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script>

function addComments(id) {
        console.log('hi');
        if (document.getElementById('comments' + id).style.display == "none") {
            document.getElementById('comments' + id).style.display = "block";
        } else {
            document.getElementById('comments' + id).style.display = "none";
        }
}

function filterArticles(id){

    category_id = id;
    
    $.ajax({
        type: 'get',
        url: '/article/filter/{'+id+'}',
        data: {
            'category_id': category_id,
            _token: '{{csrf_token()}}'
        },
        success: function (result) {
            if (result['code'] == 1) {
                    console.log(result);
                    //documnet.getElementById('c-box').style.display = "none";
                    var formoption = '';
                    var articles = result['articles'];
                    console.log(articles);
                    $.each(articles, function(v) {
                        var id = articles[v].id;
                        var title = articles[v].title;
                        var content = articles[v].content;
                        formoption += '<div class="card" style="margin-top:1%"><div class="card-header">'+title+'</div><div class="card-body">'+content+'</div><div class="card-bottom"><button onclick="addComments('+id+')"> Add Comment </button></div><div id="comments'+id+'" style="display:none"><hr><div class="row add-comment"><div class="col-md-12" style="line-height:80%"><form action="/comment/'+id+'" method="POST">@csrf<input id="commentQuestion'+id+'" class="input1 new_comment" type="text" name="content" placeholder="add your comment"></form></div></div></div></div>';
                    });

                    $('#articles').html(formoption);
                } else if (result['code'] == 0) {
                    $('#articles').html('');
                } else {
                    $('#articles').html('');
                }
            }
            , error: function(XMLHttpRequest) {
                // handle error
            }
        });
}

</script>
