@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Add Article') }}</div>

                <div class="card-body">
                    @role('admin')
            <form action="/article/store" method="POST" enctype="multipart/form-data">
            @csrf
                <div class="row">
                    <div class="col-sm-12 col-lg-12 vcenter" style="padding:1.5%">
                        <input class="input1" type="text" name="title" placeholder="Article Title">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <textarea id="editor" name="content" placeholder="Artile Content" class="input1"> </textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                    <select name="category_id" class="input1" required>
                                <option value="">Category</option>
                                @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                    </select>
                    </div>
                </div>
                <button style="float:left">Submit </button>
            </form>
                    @endrole
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
