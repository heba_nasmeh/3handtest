<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\article;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use Validator;


class ArticleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the all articles.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $articles = article::with(['comments'])->get();
        $categories = Category::all();
        return view('home', compact('articles','categories'));
    }

    /**
     * create article.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        $categories = Category::all();
        return view('article.create', compact('categories'));
    }

    /**
     * Store a new article.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'title' => ['required', 'string'],
            'content' => ['required', 'string'],
            'category_id' => ['required'],
        ]);

        if ($validator->fails()) {

            return redirect()->back()->withInput()->withErrors($validator);
        }


        $article = new article();
        $article->title = $input['title'];
        $article->content = $input['content'];
        $article->category_id = $input['category_id'];
        $article->user_id = Auth::user()->id;

        $article->save();
        return redirect()->to('home');
    }

    /**
     * edit article.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        
    }

    /**
     * Update article.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Delete article.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
    }

    /**
     * filter articles.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function filter(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'category_id' => ['required'],
        ]);

        if ($validator->fails()) {

            return redirect()->back()->withInput()->withErrors($validator);
        }

        $articles = article::where('category_id',$input['category_id'])->get();
        if($input['category_id'] == 0){
            $articles = article::all();
        }
        //$categories = Category::all();

        $data['articles'] = $articles;
        $data['message'] = 'تم استرداد المعلومات بنجاح';
        $data['code'] = 1;

        return response()->json($data, 200);
    }



}
