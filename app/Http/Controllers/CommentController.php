<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\article;
use App\Models\Category;
use App\Models\Comment;
use Illuminate\Support\Facades\Auth;
use Validator;
class CommentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the all comments.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $comments = Comment::all();
        
    }

    /**
     * create comment.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        
    }

    /**
     * Store a new comment.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $article_id)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'content' => ['required', 'string']
        ]);

        if ($validator->fails()) {

            return redirect()->back()->withInput()->withErrors($validator);
        }


        $comment = new Comment();
        $comment->content = $input['content'];
        $comment->article_id = $article_id;
        $comment->user_id = Auth::user()->id;

        $comment->save();
        return redirect()->to('home');
    }

    /**
     * edit comment.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        
    }

    /**
     * Update comment.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Delete comment.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
    }

}
